load('NeurIPS_example_fg.mat')
load('PACNL_nl_long_pacsAAAI2.mat')
%
linewidth=3;
f=figure(10);
f.Units="inches";
aspectRatio=4/2.4;
f.Position(3:4)=[linewidth,linewidth/aspectRatio]; % text width is 5.5in in neurips.


for i=1:numel(PACs)
    pl_L_hat=loglog(PACs{i}.Ns,PACs{i}.Summary.E_rho_L_hat,'r-');
    hold on
end

for i=1:numel(PACs)
    pl_2=loglog(PACs{i}.Ns,PACs{i}.Summary.Bound+PACs{i}.Summary.E_rho_L_hat,'b--');
    hold on
end
hold off
legend([pl_L_hat,pl_2],'$E_{\theta\sim\rho}\hat{\mathcal{L}}_N(\theta)$','$E_{\theta\sim\rho}\hat{\mathcal{L}}_N(\theta)+r_N$','Interpreter','latex')
grid on
xlabel('$N$','Interpreter','latex')
ax=gca
ax.XTick=10.^(0:5);
ax.YTick=10.^[-2,-1,0];
exportgraphics(gcf,'Bound.eps')



% Delta

f=figure(11);
f.Units="inches";
f.Position(3:4)=[linewidth,linewidth/aspectRatio]; % text width is 5.5in in neurips.


for i=1:numel(PACs)
    pl_L_hat=loglog(PACs{i}.Ns,abs(PACs{i}.Summary.E_rho_L_hat-PACs{i}.Summary.E_rho_L_hat(end)),'r-');
    hold on
end

for i=1:numel(PACs)
    pl_2=loglog(PACs{i}.Ns,PACs{i}.Summary.Bound,'b--');
    hold on
end
hold off
legend([pl_L_hat,pl_2],'$E_{\theta\sim\rho}(\mathcal{L}(\theta)-\hat{\mathcal{L}}_N(\theta))$','$r_N$','Interpreter','latex')
grid on
xlabel('$N$','Interpreter','latex')
ax=gca;
ax.XTick=10.^(0:5);
ax.YTick=10.^[-2,-1,0];
exportgraphics(gcf,'DeltaBound.eps')







%%
Linear=load('PACNL_nl_long_pacs_LinearComparison.mat')
f=figure(2);
f.Units="inches";
f.Position(3:4)=[linewidth/2,2]; % text width is 5.5in in neurips.


for i=1:numel(Linear.PACs)
    pl_L_hat=loglog(Linear.PACs{i}.Ns,Linear.PACs{i}.Summary.E_rho_L_hat,'r-');
    hold on
end

for i=1:numel(Linear.PACs)
    pl_2=loglog(Linear.PACs{i}.Ns,Linear.PACs{i}.Summary.Bound+Linear.PACs{i}.Summary.E_rho_L_hat,'b--');
    hold on
end

for i=1:numel(Linear.PACs)
    pl_eringis=loglog(Linear.PACs{i}.Ns,Linear.PACs{i}.Bound_Linear'+Linear.PACs{i}.Summary.E_rho_L_hat,'g--');
    hold on
end
hold off
legend([pl_L_hat,pl_2,pl_eringis],'$E_{\theta\sim\rho}\hat{\mathcal{L}}_N(\theta)$','$E_{\theta\sim\rho}\hat{\mathcal{L}}_N(\theta)+r_N$','$E_{\theta\sim\rho}\hat{\mathcal{L}}_N(\theta)+r_N$ (Eringis et.al.)','Interpreter','latex')
grid on
xlabel('$N$','Interpreter','latex')
exportgraphics(gcf,'Linear_Bound.eps')


f=figure(3);
f.Units="inches";
f.Position(3:4)=[linewidth,2.5]; % text width is 5.5in in neurips.
tiledlayout(1,2,"TileSpacing","compact","Padding","tight")
ax1=nexttile;


for i=1:numel(PACs)
    pl_L_hat=loglog(PACs{i}.Ns,PACs{i}.Summary.E_rho_L_hat,'r-');
    hold on
end

for i=1:numel(PACs)
    pl_2=loglog(PACs{i}.Ns,PACs{i}.Summary.Bound+PACs{i}.Summary.E_rho_L_hat,'b--');
    hold on
end
% yline(1)% because of tanh
hold off
grid on
xlabel('$N$','Interpreter','latex')
ax1.XTick=10.^(0:5)

ax2=nexttile;

for i=1:numel(Linear.PACs)
    pl_L_hat=loglog(Linear.PACs{i}.Ns,Linear.PACs{i}.Summary.E_rho_L_hat,'r-');
    hold on
end

for i=1:numel(Linear.PACs)
    pl_2=loglog(Linear.PACs{i}.Ns,Linear.PACs{i}.Summary.Bound+Linear.PACs{i}.Summary.E_rho_L_hat,'b--');
    hold on
end

for i=1:numel(Linear.PACs)
    pl_eringis=loglog(Linear.PACs{i}.Ns,Linear.PACs{i}.Bound_Linear'+Linear.PACs{i}.Summary.E_rho_L_hat,'g-.');
    hold on
end
% yline(Linear.PACs{i}.fg.Bq^2)
hold off
leg=legend([pl_L_hat,pl_2,pl_eringis],'$E_{\theta\sim\rho}\hat{\mathcal{L}}_N(\theta)$','$E_{\theta\sim\rho}\hat{\mathcal{L}}_N(\theta)+r_N$','$E_{\theta\sim\rho}\hat{\mathcal{L}}_N(\theta)+\tilde{r}_N$ [Eringis et.al.]','Interpreter','latex','Location','northoutside')
leg.Layout.Tile="north"
% ax1Pos=ax1.Position;
% ax2Pos=ax2.Position;
% dx=leg.Position(1)+leg.Position(3);
% leg.Position(1)=(1-2*(1-dx)-leg.Position(3))/2;
% ax1.Position=ax1Pos;
% ax2.Position=ax2Pos;
grid on
xlabel('$N$','Interpreter','latex')
ax2.XTick=10.^(0:5)
exportgraphics(gcf,'Both.eps')



