close all
clear all
clc

Nmax=1e5;%1e5;
Ns=unique(floor(logspace(0,log10(Nmax),114))); %there's 100 unique Ns when choosing 114, Nmax=1e5

figs{1}=stdFigure(1,1,...
    'Name','E_rho L_hat and (Bound+E_rho L_hat)',...
    'XData',Ns,...
    'plotFcn',{@(obj) obj.Summary.E_rho_L_hat,@(obj) obj.Summary.Bound+obj.Summary.E_rho_L_hat,@(obj) obj.Summary.E_rho_L_approx},...
    'plotLegend',{@(obj,i) sprintf('$\\omega_%i,\\; E_{f\\sim\\rho}\\hat{\\mathcal{L}}_N(f),\\; N_f=%e$',i,obj.MCMC_means.N_old),@(obj,i) sprintf('$\\omega_%i,\\; E_{f\\sim\\rho}\\hat{\\mathcal{L}}_N(f)+r_N^R$',i),@(obj,i) sprintf('$\\omega_%i,\\; E_{f\\sim\\rho}\\mathcal{L}_N(f)$',i)},...
    'Location','eastoutside',...
    'Title',@(varargin)'$\rho(f)\propto exp(-\lambda_N\hat{\mathcal{L}}_N(f))$, $\lambda_N=\sqrt{N}$');%initialised with just 1 plot
figs{end}.fig.Position=[-1150,483,766,738];
figs{end+1}=stdFigure(figs{end}.figId+1,1,...
    'Name','log(mean(Psi1)) and log(mean(Psi2))',...
    'XData',Ns,...
    'plotFcn',{@(obj) obj.Summary.log_E_pi_psi1,@(obj) obj.Summary.log_E_pi_psi2},...
    'plotLegend',{@(obj,i) sprintf('$\\omega_%i,\\; ln(E_{f\\sim\\pi} \\Psi_1(f))$',i),@(obj,i) sprintf('$\\omega_%i,\\; ln(E_{f\\sim\\pi} \\Psi_2(f)) $',i)},...
    'Location','eastoutside',...
    'Title',@(varargin)'test');
figs{end}.fig.Position=[-1918,483,766,326];
figs{end+1}=stdFigure(figs{end}.figId+1,1,...
    'Name','KL',...
    'XData',Ns,...
    'plotFcn',{@(obj) obj.Summary.KL,@(obj) obj.Summary.Z_hat_inv},...
    'plotLegend',{@(obj,i) sprintf('$\\omega_%i,\\; KL(rho|pi)$',i),@(obj,i) 'Z hat inv'},...
    'Location','eastoutside',...
    'Title',@(varargin)'test');
figs{end}.fig.Position=[-1918,895,766,326];
findfigs()
drawnow


Q=[1    0
0    1];

Bq=sqrt(2);
theta=2;
% rng(2)
fg=load('NeurIPS_example_fg.mat');
fg=fg.fg;
n_hidden_gen=2;
n_out=1;
n_in=1;

% one can generate a random RNN data generator with specified Bq and theta, and
% activation functions. 
% fg=PACNL.generate_fg(n_hidden_gen,n_in,n_out,Bq,theta,{'ReLu','tanh'});

n_hidden=2;

pacn=10;
PACs=cell(1,pacn);
for i=1:pacn
    PACs{i}=PACNL('fg',fg,'Ns',Ns','MH_thin',2,'propStep',0.01*eye(14)); %0.0605*eye(14)
    PACs{i}.set_fg(fg);
    PACs{i}.generateData(Nmax,'Trunc_Gaussian',Q); % |e|_inf is defined by the desired Bq and theta when generating fg
    PACs{i}.setPrior('Custom',@log_prior) % custom prior, defined at the bottom of this script
    PACs{i}.setPosterior('Gibbs',@(N) sqrt(N)); % using gibbs posterior with lambda_N=sqrt(N)
    PACs{i}.set_lambdas(@(N) sqrt(N)); %lambda in the bound is set to sqrt(N)

    PACs{i}.LossFcn.forward=@(y,y_hat) sum((y-y_hat).^2);
    PACs{i}.LossFcn.Lip=@(G,H,Bq,theta) Bq*G;

    % Specifying the structure of the predictor, i.e. 2 states, 1 input, 1
    % output, activation on the states: 'identity', activation on output:
    % 'identity', other choices for act.fcn. are: 'ReLu','tanh'
    f=PACs{i}.set_f0(n_hidden,n_in,n_out,{'ReLu','tanh'});
    % returns a initial predictor, but it is stored internally so no need
    % to use it, it's just to print it out in a command window to see.


    % regularising the initial predictor, this will have almost no effect
    % due to use of burn-in in the mhsample step. 
    pvec=f.pvec;
    pvec([9,10,14])=0; % bias terms are set to 0 for initial predictor
    pvec=sign(pvec).*min(abs(pvec),0.99); % \forall i |theta_i|<=0.99
    f0=PACNL.setpvec(PACs{i}.f0,pvec);
    PACs{i}.set_f0(f0);
%     PACs{i}.MCMC_options.propStep([9,10,14],[9,10,14])=0; % bias terms are fixed;


end


%%
Nf=500;

for i=1:1000

[fs_prior,SysConstants,smpl]=PACs{1}.improveMCestimate(Nf);
    for j=1:numel(figs)
        try
        figs{j}.updatePlots(PACs);
        catch
        end
    end
    plotpvecHists(PACs{1},figs{end}.figId+1)
    drawnow
for k=2:numel(PACs)
    PACs{k}.improveMCestimate(Nf,fs_prior,SysConstants,smpl);
    for j=1:numel(figs)
        try
        figs{j}.updatePlots(PACs);
        catch
        end
    end
    plotpvecHists(PACs{1},figs{end}.figId+1)
    drawnow
end
    save('PACNL_nl_long_pacsAAAI2.mat','PACs');
end



function lpdf= log_prior(f) %Script checks if f is of class S before calling this function
% if f is not in class lpdf is set to -inf;


% if you wish to use the constants of the predictor in the prior here is an example:
%     lpdf=-10*(f.consts.Lhx*f.consts.Lv/(1-f.consts.lambda)+f.consts.Lhv);

    mu=zeros(numel(f.pvec),1);
    Sigma=0.02*eye(numel(f.pvec));
    lpdf=log(mvnpdf(f.pvec,mu,Sigma));
end