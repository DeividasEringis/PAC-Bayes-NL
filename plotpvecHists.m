function plotpvecHists(obj,figId)
smpl=obj.Summary.MCMCsummary.smpl;
f=figure(figId);
f.Position=[2561,146,1920,1083];
n_th=size(smpl,2);
x=obj.f0.n+obj.f0.np;
y=obj.f0.n+obj.f0.nu+2;
tiledlayout(x,y,'TileSpacing','tight')



n=obj.f0.n;
nu=obj.f0.nu;
np=obj.f0.np;
idx=[];
for row=1:n
    A_idx=row*n+(1:n);
    B_idx=n+n^2+(row-1)*nu+(1:nu);
    b1_idx=n+n^2+n*nu+row;
    x0_idx=row;
    idx=horzcat(idx,A_idx,B_idx,b1_idx,x0_idx);
end

for row=1:nu
C_idx=n+n^2+n*nu+row*n+(1:n);
D_idx=n+n^2+n*nu+n+np*n+(row-1)*nu+(1:nu);
b2_idx=n+n^2+n*nu+n+np*n+np*np+row;
idx=horzcat(idx,C_idx,D_idx,b2_idx);
end

w=obj.Summary.MCMCsummary.betas(:,end)/obj.Summary.Z_hat_inv(end);
for i=idx
    nexttile;
    h=histogram(smpl(:,i),'Normalization','pdf');
    hold on
    histw=histwv(smpl(:,i), w, h.BinEdges, h.NumBins);
    histogram('BinEdges',h.BinEdges,'BinCounts',histw,'Normalization','pdf')
%     [v,bin_cntr]=hist(smpl(:,i));
%     dx=bin_cntr(1)-min(smpl(:,i));
%     bin_edges=[min(smpl(:,i)),bin_cntr+dx];
%     histw=histwv(smpl(:,i),w,min(smpl(:,i)),max(smpl(:,i)),numel(v));
%     
%     hold on
%     
%     hold off
    if i<=obj.f0.n
        title(sprintf('$x_{0,%i}$',i),'Interpreter','latex')
    elseif i<=n+n^2
        j=i-n;
        a=floor((j-1)/n)+1;
        b=mod((j-1),n)+1;
        title(sprintf('$A_{%i,%i}$',a,b),'Interpreter','latex')
    elseif i<=n+n^2+n*nu
        j=i-n-n^2;
        a=floor((j-1)/nu)+1;
        b=mod((j-1),nu)+1;
        title(sprintf('$B_{%i,%i}$',a,b),'Interpreter','latex')
    elseif i<=n+n^2+n*nu+n
        j=i-n-n^2-n*nu;
        title(sprintf('$b_{1,%i}$',j),'Interpreter','latex')
    elseif i<=n+n^2+n*nu+n+np*n
        j=i-(n+n^2+n*nu+n);
        a=floor((j-1)/n)+1;
        b=mod((j-1),n)+1;
        title(sprintf('$C_{%i,%i}$',a,b),'Interpreter','latex')
    elseif i<=n+n^2+n*nu+n+np*n+np*np
        j=i-(n+n^2+n*nu+n+np*n);
        a=floor((j-1)/np)+1;
        b=mod((j-1),np)+1;
        title(sprintf('$D_{%i,%i}$',a,b),'Interpreter','latex')
    elseif i<=n+n^2+n*nu+n+np*n+np*np+np
        j=i-(n+n^2+n*nu+n+np*n+np*np);
        title(sprintf('$b_{2,%i}$',j),'Interpreter','latex')
    end

end
end

function [histw, histv] = histwv(v, w, edges, bins)
%Inputs:
%vv - values
%ww - weights
%minV - minimum value
%maxV - max value
%bins - number of bins (inclusive)

%Outputs:
%histw - wieghted histogram
%histv (optional) - histogram of values

% delta = (max-min)/(bins-1);
% subs = round((v-min)/delta)+1;
Q=((v>=edges(1:end-1))&(v<edges(2:end)));
[k,j]=find(Q);


histv = accumarray(j,1,[bins,1]);
histw = accumarray(j,w(k),[bins,1]);
end