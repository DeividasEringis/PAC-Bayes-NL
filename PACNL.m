classdef PACNL < handle
    %UNTITLED5 Summary of this class goes here
    %   Detailed explanation goes here

    properties
        y=[];
        w=[];
        Ns=[10,20];
        f0;
%         f0_burnin;
        Summary=struct('E_rho_L_hat',0,'E_rho_L',0,'Z_hat_inv',0,'KL',0,...
            'MCMCsummary',struct('absL_L_hat',0,...
                                'absL_L_hat_delta',0,...
                                'sigma_nabla',0,...
                                'smpl',[],'betas',[]));
        MCMC_means=struct('E_rho_L_hat',0,...
                          'Erho_L',0,...
                          'Z_hat_inv',0,...
                          'V',0,... % E_pi (Beta(f).*L_hat_Ns)
                          'N_old',0);
        MCMC_options=struct('log_prior_fcn',@(x) -inf,'proprnd_fcn', @(f,step) f,'propStep',9.55e-03,'MH_thin',1);
        LossFcn=struct('forward',@(y,y_hat) sum((y-y_hat).^2),'lip',@(G,H,Bq,theta) Bq*G);        
        lambda_PAC=sqrt([10,20]);
        Posterior=struct('Type','Gibbs','lambda_rho',sqrt([10,20]),'BetaScaling',[]);
        BoundConsts=struct('delta',0.1);
        NLBounds;
        fg=struct('sys',[],'Bq',1,'theta',1);
        UserInput=struct;
        Ge_priors=[];
        Gf_priors=[];
        Ge1_priors=[];
        barGf1_priors=[];
        barGf2_priors=[];
        Bound_Linear=[];
        computeEringisBound=false;
    end

    properties(Constant)
        BasisActFcns=struct('ReLu',struct('forward',@(x)x.*(x>0),'lip',1),'tanh',struct('forward',@tanh,'lip',1),'ident',struct('forward',@(x)x,'lip',1));
    end
%     properties(Access=private)
%         Quants={{@(f,fg) normL1(find_fe(f,fg));@(f,fg) normL1_alternative(find_fe(f,fg));@(f) f.barGf1;@(f) f.barGf2},...
%                 {@(f,fg) normL1(find_fe(f,fg));@(f) f.g}};
%     end

    methods
        function obj = PACNL(varargin) %Ns,y,w,delta,fg,f0,log_prior_fcn,proprnd_fcn,propStep,MH_thin,f_opt,
            %PAC Construct an instance of this class
            %   Detailed explanation goes here
            
            for i=1:2:numel(varargin)
                obj.UserInput.(varargin{i})=varargin{i+1};
            end
            
            toSet={'Ns','y','w','fg','f0','BoundConsts.delta','MCMC_options.log_prior_fcn','MCMC_options.proprnd_fcn','MCMC_options.propStep','MCMC_options.MH_thin'};
            for i=1:numel(toSet)
                loc=strsplit(toSet{i},'.');
                try
                    q=cell(1,numel(loc));
                    q{1}=obj;
                    for j=1:numel(loc)-1
                        q{j+1}=q{j}.(loc{j});
                    end
                    q{end}.(loc{end})=obj.UserInput.(loc{end});
                    for j=numel(q):-1:2
                        q{j-1}.(loc{j-1})=q{j};
                    end
                    obj=q{1};
                catch ME
                    if strcmp(ME.identifier,'MATLAB:nonExistentField')
                        q=obj;
                        for j=1:numel(loc)-1
                            q=q.(loc{j});
                        end
                        default=q.(loc{end});
                        if isnumeric(default) && numel(default)==1
                            warning('Did not provide %s, using default setting: %s=%f',loc{end},loc{end},default);
                        else
                            warning('Did not provide %s, using default setting.',loc{end});
                        end
                    else
                        warning('Tried assigning %s, got: %s\n',toSet{i},ME.message)
                    end
                end
            end
%             obj.Ns=Ns;
%             obj.MCMC_means.V=zeros(numel(Ns),1);
%             obj.V_L=zeros(numel(Ns),1);
%             obj.V_L_L_hat=zeros(numel(Ns),1);
%             obj.Z_hat_inv=zeros(numel(Ns),1);
%             obj.absL_L_hat=zeros(numel(Ns),1);
%             obj.N_old=0;
%             obj.f0_burnin=false;
            if isempty(obj.fg) && isempty(obj.y)
                obj.fg=obj.genSys(2,1,2,{obj.BasisActFcns.ident,obj.BasisActFcns.ident});
            end
            if isempty(obj.f0)
                obj.f0=obj.genSys(2,1,1,{obj.BasisActFcns.ident,obj.BasisActFcns.ident});
            end
            obj.MCMC_options.log_prior_fcn=@obj.log_prior;
            
        end

        function pvec=proprnd(~,pvec,mag)
%             f=f0;
            if numel(mag)==1
                Sigma=eye(size(pvec)).*mag;
            else
                Sigma=mag;
            end
            pvec=pvec+mvnrnd(zeros(size(pvec)),Sigma);
%             Weights = obj.getWeightsFromPvec(f.pvec,f.n,f.nu,f.np);
%             f.xEnd=Weights.x0;
%             f.consts=obj.getSysConsts(f);
%             f.forward=@(x,u,varargin) PACNL.default_Forward_RNN(x,u,f.pvec,f.n,f.nu,f.np,f.actFcns,varargin);
        end

        function lpdf= log_prior(obj,pvec) %input is row vector from mhsample obj
            f=obj.sysFromPvec(pvec');
            if obj.isOfClass(f)
                lpdf=-10*(f.consts.Lhx*f.consts.Lv/(1-f.consts.lambda)+f.consts.Lhv);
            else
                lpdf=-inf;
            end
        end

        function f=set_f0(obj,varargin)
            % Usage: n,nu,np,actFcnNames
            if nargin==2
                obj.f0=varargin{1};
            else
                n=varargin{1};
                nu=varargin{2};
                np=varargin{3};
                actFcnNames=varargin{4};
                actFcns=cell(2,1);
                for i=1:2
                    actFcns{i}=obj.BasisActFcns.(actFcnNames{i});
                end
                obj.f0=obj.genSys(n,nu,np,actFcns);
                f=obj.f0;
            end
        end

        function setPrior(obj,name,val)
            obj.MCMC_options.log_prior_fcn=@(pvec) obj.custom_log_prior(pvec,val);
        end

        function lpdf=custom_log_prior(obj,pvec,fun)
            f=obj.sysFromPvec(pvec');
            if PACNL.isOfClass(f)
                lpdf=fun(f);
            else
                lpdf=-inf;
            end
        end

        function f=sysFromPvec(obj,pvec)
            f=obj.f0;
            f.pvec=pvec;
            Weights = obj.getWeightsFromPvec(f.pvec,f.n,f.nu,f.np);
            f.xEnd=Weights.x0;
            f.consts=obj.getSysConsts(f);
            f.forward=@(x,u,varargin) PACNL.default_Forward_RNN(x,u,f.pvec,f.n,f.nu,f.np,f.actFcns,varargin);
        end







        function smpl = samplePvecFromPrior(obj,f0,N,log_prior)
            if numel(obj.MCMC_options.propStep)==1
%                 obj.MCMC_options.propStep=2;
                obj.estPropRndSigma(f0.pvec',log_prior)
            end
%             [smpl,acc]=mhsample(f0.pvec',N,'logpdf',log_prior,'proprnd',@(x) obj.proprnd(x,obj.MCMC_options.propStep),'symmetric',1,'burnin',N,'thin',obj.MCMC_options.MH_thin);
%             fprintf('mhsample: sampled %i samples, with acceptance prob=%5.2f%%\n',N,acc*100);
            
            
            acc=0;
            th=10;
            while abs(acc-23.4)>th
            [smpl,acc]=mhsample(f0.pvec',N,'logpdf',log_prior,'proprnd',@(x) obj.proprnd(x,obj.MCMC_options.propStep),'symmetric',1,'burnin',N);
            acc=acc*100;
            if abs(acc-23.4)>th
                oldPropStep=obj.MCMC_options.propStep;
                obj.MCMC_options.propStep=oldPropStep*(1+0.5*(acc-23.4)/100);
                fprintf(2,'mhsample acc=%5.2f%%, not nominal, changed |propStep| from %5.2e to %5.2e\n',acc,norm(oldPropStep,2),norm(obj.MCMC_options.propStep,2));
            else
                fprintf('mhsample completed with acc=%5.2f%%\n',acc);
                obj.MCMC_options.propStep=obj.MCMC_options.propStep*(1+0.1*(acc-23.4)/100);
            end
            end
        end

        function estPropRndSigma(obj,x0,lpdf)
            fprintf('Estimating PropRnd Sigma\n')
            [smpl,~]=mhsample(x0,1e3,'logpdf',lpdf,'proprnd',@(x) obj.proprnd(x,obj.MCMC_options.propStep),'symmetric',1,'burnin',100,'thin',obj.MCMC_options.MH_thin);
            x0=smpl(end,:);
            Sigma=cov(smpl);
            Sigma=Sigma/norm(Sigma,2);
            Sigma(diag(ones(size(Sigma,1),1))==1)=max(diag(Sigma),1e-2);
            for i=1:1
                acc=0;
                th=5;
                while abs(acc-23.4)>th
                    [smpl,acc]=mhsample(x0,2e3,'logpdf',lpdf,'proprnd',@(x) obj.proprnd(x,obj.MCMC_options.propStep*Sigma),'symmetric',1,'burnin',100,'thin',obj.MCMC_options.MH_thin);
                    x0=smpl(end,:);
                    Sigma=cov(smpl);
                    Sigma(diag(ones(size(Sigma,1),1))==1)=max(diag(Sigma),1e-1);
                    Sigma=Sigma/norm(Sigma,2);
                    acc=acc*100;
                    if abs(acc-23.4)>th
                        oldPropStep=obj.MCMC_options.propStep;
%                         if acc-23.4>0
%                             obj.MCMC_options.propStep=oldPropStep*1.2;
%                         else
%                             obj.MCMC_options.propStep=oldPropStep*0.8;
%                         end
                        obj.MCMC_options.propStep=oldPropStep*(1+0.66*min(max(10*(acc-23.4)/100,-0.5),1));
                        fprintf(2,'SigmaEst: mhsample acc=%6.2f%%, not nominal, changed propStep from %5.2e to %5.2e\n',acc,oldPropStep,obj.MCMC_options.propStep);
                    else
                        %double check the acc prob:
                        [~,acc]=mhsample(x0,1e3,'logpdf',lpdf,'proprnd',@(x) obj.proprnd(x,obj.MCMC_options.propStep*Sigma),'symmetric',1,'burnin',100);
                        acc=acc*100;
                        if abs(acc-23.4)<=th
                            fprintf('PropRndSigma estimated, obtained acceptable probability:%5.2f%%\n',acc);
                        end
                    end
                end
            end
            obj.MCMC_options.propStep=obj.MCMC_options.propStep*Sigma;
        end


        function [fs_prior,SysConstants,smpl]=improveMCestimate(obj,Nf_it,varargin)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            if nargin<=2
                smpl=obj.samplePvecFromPrior(obj.f0,Nf_it,obj.MCMC_options.log_prior_fcn);
                obj.f0=obj.sysFromPvec(smpl(end,:)');
                fs_prior=cell(Nf_it,1);
                SysConstants=zeros(Nf_it,5);
                L_hats_Ns=zeros(Nf_it,numel(obj.Ns));
                if isempty(obj.y)
                        error('No dataset specified, either specify a generator, or specify [y,w]')
                else
                    empLossFcn=@(f)PACNL.empLoss_prototype(obj.LossFcn.forward,f,obj.y,obj.w,obj.Ns);
                    sysFromPvec_=@(x)obj.sysFromPvec(x);
                    parfor i=1:Nf_it
                        fs_prior{i}=sysFromPvec_(smpl(i,:)');
                        sysconst=[fs_prior{i}.consts.C,fs_prior{i}.consts.lambda,fs_prior{i}.consts.Lv,fs_prior{i}.consts.Lhx,fs_prior{i}.consts.Lhv];
                        SysConstants(i,:)=sysconst;
                        L_hats_Ns(i,:)=empLossFcn(fs_prior{i});
                    end
                end
                if obj.computeEringisBound==true
                    Ge_priors_new=zeros(numel(fs_prior),1);
                    Gf_priors_new=zeros(numel(fs_prior),1);
                    Ge1_priors_new=zeros(numel(fs_prior),1);
                    barGf1_priors_new=zeros(numel(fs_prior),1);
                    barGf2_priors_new=zeros(numel(fs_prior),1);
                    fg_=obj.fg;
                    for i=1:numel(fs_prior)
                        fe=PACNL.find_fe(fs_prior{i},fg_);
                        Ge_priors_new(i)=PACNL.normL1(fe);%(cellfcn(@(f) normL1(find_fe(f,fgs{q})),fs_prior{q}));  
                        [Gf_priors_new(i),~,~,barGf1_priors_new(i),barGf2_priors_new(i)]=PACNL.Gf_new(fs_prior{i});
                        Ge1_priors_new(i)=PACNL.normL1_alternative(fe);
                    end
                    SysConstants(:,end+1)=Ge_priors_new;
                    SysConstants(:,end+1)=Gf_priors_new;
                    SysConstants(:,end+1)=Ge1_priors_new;
                    SysConstants(:,end+1)=barGf1_priors_new;
                    SysConstants(:,end+1)=barGf2_priors_new;
                end
%                 [Constants_,ConstantsNs,~,~,obj.f0,fs_prior_new]=sampleConstants_from_prior(Nf_it,obj.Ns,obj.y,obj.w,obj.f0,obj.fg,obj.log_prior_fcn,obj.proprnd_fcn,obj.propStep,obj.MH_thin);
%                 obj.fs_prior=fs_prior_new;

%                 obj.Constants=Constants_;
            else
                fs_prior=varargin{1};
                SysConstants=varargin{2};
                if size(SysConstants,2)>5
                    Ge_priors_new=SysConstants(:,end-4);
                    Gf_priors_new=SysConstants(:,end-3);
                    Ge1_priors_new=SysConstants(:,end-2);
                    barGf1_priors_new=SysConstants(:,end-1);
                    barGf2_priors_new=SysConstants(:,end);
                    SysConstants=SysConstants(:,1:end-5);
                end
                smpl=varargin{3};
                L_hats_Ns=zeros(Nf_it,numel(obj.Ns));
                if isempty(obj.y)
                    error('No dataset specified, either specify a generator, or specify [y,w]')
                else
                    empLossFcn=@(f)PACNL.empLoss_prototype(obj.LossFcn.forward,f,obj.y,obj.w,obj.Ns);
                    parfor i=1:Nf_it
                        L_hats_Ns(i,:)=empLossFcn(fs_prior{i});
                    end
                end
            end

            new_Betas=obj.getBetas(1:numel(obj.Ns),fs_prior,L_hats_Ns');
            
            obj.updateMean('Z_hat_inv',new_Betas);
            obj.Summary.Z_hat_inv=obj.MCMC_means.Z_hat_inv+eps(obj.MCMC_means.Z_hat_inv);

            obj.updateMean('E_pi_Beta_log_Beta',new_Betas.*log(new_Betas+eps(new_Betas)));
            obj.updateMean('V',new_Betas.*L_hats_Ns');
            obj.updateMean('V_L',new_Betas.*L_hats_Ns(:,end)')


            % NL PAC
            C=SysConstants(:,1);
            lambdas_sys=SysConstants(:,2);
            Lv=SysConstants(:,3);
            Lhx=SysConstants(:,4);
            Lhv=SysConstants(:,5);
            G=Lhv+Lhx.*Lv./(1-lambdas_sys); 
            H=Lhx.*Lv./((1-lambdas_sys).^2);
            Lls=obj.fg.Bq.*G; %%default for mse L_\ell
            for i=1:Nf_it
                if isa(obj.LossFcn.Lip,'function_handle')==true
                    Lls(i)=obj.LossFcn.Lip(G(i),H(i),obj.fg.Bq,obj.fg.theta);
                else
                    Lls(:)=obj.LossFcn.Lip;
                end
            end
            lambdas=obj.lambda_PAC; %note it is different from obj.Posterior.lambda_rho
            args=(obj.fg.Bq+obj.fg.theta).*G+obj.fg.Bq.*(Lhx.*Lv./((1-lambdas_sys).^2));
            NL_Psi1_args=(2*lambdas.^2./obj.Ns)'.*(Lls.*args).^2;
            obj.updateLSEMean('log_E_pi_psi1',NL_Psi1_args')
%             obj.updateMean('E_pi_psi1',exp(NL_Psi1_args)')

            x0_norms=zeros(Nf_it,1);
            for i=1:Nf_it
                x0_norms(i)=norm(fs_prior{i}.x0(fs_prior{i}.pvec),2);
            end
            p1=2*obj.fg.Bq*Lv./((1-lambdas_sys).^2);
            p2=x0_norms./(1-lambdas_sys);
            NL_Psi2_args=(2*lambdas./obj.Ns)'.*(Lls.*Lhx.*C.*(p1+p2));
            obj.updateLSEMean('log_E_pi_psi2',NL_Psi2_args')
%             obj.updateMean('E_pi_psi2',exp(NL_Psi2_args)')

            
            Z_hat_inv=obj.MCMC_means.Z_hat_inv+eps(obj.MCMC_means.Z_hat_inv);
            KL=-log(Z_hat_inv)+obj.MCMC_means.E_pi_Beta_log_Beta./Z_hat_inv;
            obj.Summary.KL=KL;
            Psi=0.5*(obj.MCMC_means.log_E_pi_psi1+obj.MCMC_means.log_E_pi_psi2);
            obj.Summary.log_E_pi_psi1=obj.MCMC_means.log_E_pi_psi1;
            obj.Summary.log_E_pi_psi2=obj.MCMC_means.log_E_pi_psi2;
            
            obj.NLBounds=(KL+log(1/obj.BoundConsts.delta)+Psi)./lambdas;
            obj.Summary.Bound=obj.NLBounds;
            
            if any(isinf(obj.NLBounds))||any(isnan(obj.NLBounds))
                fprintf(2,'inf or nan detected\n')
            end


            if obj.computeEringisBound==true
                % [Eringis et.al.,2023] Bounds for linear systems
                lse=@(x) max(x)+log(sum(exp(x-max(x))));
                obj.Ge_priors=[obj.Ge_priors,Ge_priors_new'];
                obj.Gf_priors=[obj.Gf_priors,Gf_priors_new'];
                obj.Ge1_priors=[obj.Ge1_priors,Ge1_priors_new'];
                obj.barGf1_priors=[obj.barGf1_priors,barGf1_priors_new'];
                obj.barGf2_priors=[obj.barGf2_priors,barGf2_priors_new'];
                Nf=numel(obj.Ge_priors);
                for i=1:numel(obj.Ns)
                    N=obj.Ns(i);
                    lambda=lambdas(i);
                 
                    C=obj.fg.ce*sqrt(obj.fg.p+obj.fg.q);
                    lambda_tilde=lambda/sqrt(N);
                    
                   Psi1=log(mean(exp(lambda_tilde^2.*2.*(obj.Ge_priors+obj.Ge1_priors).^2.*C^2.*(4.*obj.Ge_priors.*C+1).^2)));
                    
                    temp_G=obj.barGf1_priors.*PACNL.normL1(obj.fg)*C;
                    
    
                    lambda_tilde2=lambda/N;
                    Psi2=log(mean(1-temp_G+temp_G.*exp((lambda_tilde2.*8.*PACNL.normL1(obj.fg).*C.*obj.barGf2_priors))));
                    
    
                    Psi=0.5*(Psi1+Psi2);
                    if isinf(Psi)
                        Psi2=log(mean(1-temp_G+temp_G.*exp(vpa(lambda_tilde2.*8.*PACNL.normL1(obj.fg).*C.*obj.barGf2_priors))));
                        Psi1=log(mean(exp(vpa(lambda_tilde^2.*2.*(obj.Ge_priors+obj.Ge1_priors).^2.*C^2.*(4.*obj.Ge_priors.*C+1).^2))));
                        Psi=0.5*(Psi1+Psi2);
                    end
                    obj.Bound_Linear(i)= (KL(i) + log(1/obj.BoundConsts.delta) + Psi)/lambda;
                end
            end





            
            % General stuff. 
            obj.MCMC_means.E_rho_L_hat=obj.MCMC_means.V./obj.MCMC_means.Z_hat_inv;
            obj.Summary.E_rho_L_hat=obj.MCMC_means.E_rho_L_hat;
            obj.Summary.E_rho_L_approx=obj.MCMC_means.V_L./obj.MCMC_means.Z_hat_inv;
            obj.Summary.MCMCsummary.betas=vertcat(obj.Summary.MCMCsummary.betas,new_Betas');
            obj.Summary.MCMCsummary.smpl=vertcat(obj.Summary.MCMCsummary.smpl,smpl);
            obj.MCMC_means.N_old=obj.MCMC_means.N_old+Nf_it;
        end

        function updateMean(obj,mean,newVals)
            n=size(newVals,2);
            if isfield(obj.MCMC_means,mean)==false
                obj.MCMC_means.(mean)=0;
            end
            obj.MCMC_means.(mean)=obj.MCMC_means.(mean)*(obj.MCMC_means.N_old/(obj.MCMC_means.N_old+n)) +sum(newVals,2)/(obj.MCMC_means.N_old+n);
        end

        function updateLSEMean(obj,mean,newVals)
            n=size(newVals,2);
            if isfield(obj.MCMC_means,mean)==false
                obj.MCMC_means.(mean)=-inf*ones(size(newVals,1),1);
            end
            N_old=obj.MCMC_means.N_old;
            v=[obj.MCMC_means.(mean)+log(N_old/(N_old+n)),newVals-log(N_old+n)];
            x_bar=max(v,[],2);
            obj.MCMC_means.(mean)=x_bar+log(sum(exp(v-x_bar),2));
%             obj.MCMC_means.(mean)=obj.MCMC_means.(mean)*(/(obj.MCMC_means.N_old+n)) +sum(newVals,2)/(obj.MCMC_means.N_old+n);
        end

        function Bounds=computeBounds(obj)
            % computes Renyi PAC-Bayesian bound for all N\in Ns at the same
            % time.
            % returns a vector for r_N^R, 

%             obj.BoundConsts.EGf2=mean(Gf_priors.^2); %Scalar.
%             obj.BoundConsts.EGe4=mean(Ge_priors.^4); %Scalar.
%             obj.BoundConsts.Z_hat_inv_=mean(betas,2); %col vector, one for each N in Ns
%             obj.BoundConsts.eRenyi_arg=mean(betas.^2,2);%col vector
            sqrt_eRenyi=sqrt(obj.BoundConsts.eRenyi_arg)./obj.BoundConsts.Z_hat_inv_;
            
            sqrtNs=sqrt(obj.Ns);
            c1=obj.fg.mQ.*sqrt(factorial(obj.fg.p+obj.fg.q+1))./(sqrt(obj.BoundConsts.delta).*sqrtNs);
            c2=sqrt_eRenyi;
            c3=( 4*obj.fg.l1_sqr*sqrt(obj.BoundConsts.EGf2)./sqrtNs) +   6*obj.fg.p*sqrt(obj.BoundConsts.EGe4) ;
            Bounds=c1'.*c2.*c3';


        end


        function Linear_Bounds= computeLinear_boundedCase_Bounds(obj)
            KL=approxKLpi(Betas);
        
            G_gen_1=8*ce^2*fg.p*(fg.p+fg.q);
            G_gen_2=4*ce^2*(fg.p+fg.q)*fg.l1_sqr;
        
            %log(Efpi 1+ e^(G_gen_1*Ge(f))/(2*N)+e^(G_gen_2*Gf(f))/(2*N))
            %= log(1 + Efpi e^(G_gen_1*Ge(f))/(2*N) + Efpi e^(G_gen_2*Gf(f))/(2*sqrt(N))
            %~=log(1 + \sum_i e^(G_gen_1*Ge(f_i)-log(2*N*Nf)) + \sum_i e^(G_gen_2*Gf(f_i)-log(2*sqrt(N)*Nf))
            Nf=numel(Ge_priors);
            %     Psi=lse([0, lambda.*G_gen_1.*Ge_priors.^2-log(2)-log(N)-log(Nf), lambda.*G_gen_2.*Gf_bounded_priors-log(2)-0.5*log(N)-log(Nf) ]);
            Psi1=lse([0, lambda.*G_gen_1.*Ge_priors.^2-log(N)-log(Nf)]);
            Psi2=lse([0, lambda.*G_gen_2.*Gf_bounded_priors-0.5*log(N)-log(Nf)]);
            Psi=0.5*(Psi1+Psi2);
            Bound= (KL + log(1/delta) + Psi)/lambda;
        end

        function setPosterior(obj,type,magnitude)
            % types: 'Gibbs',lambda
            %       'optCentered',Sigma_inv_Scale

            if isa(magnitude,'function_handle')
                fcn=magnitude;
                magnitude=zeros(numel(obj.Ns),1);
                for i=1:numel(obj.Ns)
                    magnitude(i)=fcn(obj.Ns(i));
                end
            else
                if numel(magnitude)==1
                    magnitude=magnitude.*ones(numel(obj.Ns),1);
                end
            end

            if strcmp(type,'Gibbs')
                obj.Posterior.Type='Gibbs';
                obj.Posterior.lambda_rho=magnitude;
            elseif strcmp(type,'optCentered')
                obj.Posterior.Type='optCentered';
                obj.Posterior.SigmaMag=magnitude;
            end
        end
        function set_lambdas(obj,magnitude)
            % types: 'Gibbs',lambda
            %       'optCentered',Sigma_inv_Scale

            if isa(magnitude,'function_handle')
                fcn=magnitude;
                magnitude=zeros(numel(obj.Ns),1);
                for i=1:numel(obj.Ns)
                    magnitude(i)=fcn(obj.Ns(i));
                end
            else
                if numel(magnitude)==1
                    magnitude=magnitude.*ones(numel(obj.Ns),1);
                end
            end
            obj.lambda_PAC=magnitude;
        end

        function Betas = getBetas(obj,i,fs_prior,L_hat_priorsNs_)
            % returns betas\propto \rho(f)/\pi(f)
            % i is an index to obj.Ns array
            % if Posterior.Type=='optCentered':
            %       x=obj.fs_prior is a cell array of 1 or 2 dimensions
            % if Posterior.Type=='Gibbs'
            %       x=obj.L_hat_priorsNs is a matrix of 1 or 2 dimensions
            %       containing empirical loss samples from prior.

            
            if strcmp(obj.Posterior.Type,'Gibbs')
                if isempty(obj.Posterior.BetaScaling)
                    obj.Posterior.BetaScaling=1/mean(mean(exp((-obj.Posterior.lambda_rho.*L_hat_priorsNs_))));
                    if obj.Posterior.BetaScaling>1e30
                        obj.Posterior.BetaScaling=1.0e30;
                    end
                    fprintf('Artificial Beta scaling set to %6.2e, to help floating point computations \n',obj.Posterior.BetaScaling)
                end
                Betas=obj.Posterior.BetaScaling*exp((-obj.Posterior.lambda_rho(i).*L_hat_priorsNs_(i,:)));
            elseif strcmp(obj.Posterior.Type,'optCentered')
                Betas=obj.optCenteredBeta(fs_prior,i);
            end
        end

        function betas = optCenteredBeta(obj,fs,idx)
            % beta \propto \frac{\rho}{\pi}
            % \hat{\rho}=exp(-(\theta-\theta_0)^T\Sigma^{-1}(\theta-\theta_0))
            % \hat{\pi}=exp(log_prior_fcn(f))

            theta_0=[reshape(obj.f_opt.A,[],1);reshape(obj.f_opt.B,[],1);reshape(obj.f_opt.C,[],1);reshape(obj.f_opt.D,[],1)];
            
            betas=zeros(numel(idx),numel(fs));
            for i=1:numel(idx)
                mag=obj.Posterior.SigmaMag(i);
                Sigma_inv=mag*diag(ones(numel(theta_0),1));
                l_prior=obj.log_prior_fcn;
                if i==1 || obj.Posterior.SigmaMag(i)~=obj.Posterior.SigmaMag(i-1)
                    parfor j=1:size(fs,2)
                        theta=[reshape(fs{j}.A,[],1);reshape(fs{j}.B,[],1);reshape(fs{j}.C,[],1);reshape(fs{j}.D,[],1)];
                        d=theta-theta_0;
                        betas(i,j)=exp(-d'*Sigma_inv*d-l_prior(fs{j}));
                    end
                else
                    betas(i,:)=betas(i-1,:);
                end
            end

        end

        function generateData(obj,Nmax,noiseType,varargin)
        %%generateData Generates input/output data
        % Options: 'Gaussian','Trunc_Gaussian'
            fprintf('Generating [y,w] \n Please wait... \n')
            p=obj.fg.p;
            q=obj.fg.q;
            v_init=nan(p+q,1e3);
            v=nan(p+q,Nmax);
            if nargin>3
                Q=varargin{1};
            else
                Q=obj.fg.Q;
            end
            R=chol(Q)';
            
            if strcmp(noiseType,'Gaussian')
                    v_init=R*randn(p+q,1e3);
                    v=R*randn(p+q,Nmax);
            elseif strcmp(noiseType,'Trunc_Gaussian')
%                     obj.fg.ce=varargin{2};
                    ce_=obj.fg.ce/max(sum(R,2));
                    pd = makedist('Normal');
                    t = truncate(pd,-ce_,ce_);
                    r = random(t,p+q,1e3);
                    v_init=R*r;
                    r = random(t,p+q,Nmax);
                    v=R*r;
            end
            
            xt=randn(2,1);
            
            
            
            % [xt,q(:,1),A,B,b1,C,D,b2]=obj.fg.forward(xt,eg(:,i));
            Weights=obj.getWeightsFromPvec(obj.fg.pvec,obj.fg.n,obj.fg.nu,obj.fg.np);
            for i=1:1e3
                [xt,~]=obj.fg.forward(xt,v_init(:,i),Weights.A,Weights.B,Weights.b1,Weights.C,Weights.D,Weights.b2);
            end

%             x=zeros(n,1);
%             for i=1:size(v,2)
%                x=obj.fg.A*x+obj.fg.B*v_init(:,i);
%             end
            y_=zeros(p,Nmax);
            w_=zeros(q,Nmax);

%             A=obj.fg.A;
%             B=obj.fg.B;
%             Cy=obj.fg.C(1:p,:);
%             Cw=obj.fg.C(p+1:end,:);
%             Dy=obj.fg.D(1:p,:);
%             Dw=obj.fg.D(p+1:end,:);
            q=nan(p+q,Nmax);
            for i=1:Nmax
%                y_(:,i)=Cy*x+Dy*v(:,i);
%                w_(:,i)=Cw*x+Dw*v(:,i);
%                x=A*x+B*v(:,i);
               [xt,q(:,i)]=obj.fg.forward(xt,v(:,i),Weights.A,Weights.B,Weights.b1,Weights.C,Weights.D,Weights.b2);
            end
            obj.y=q(1:p,:);
            obj.w=q(p+1:end,:);
            fprintf('Done generating [y,w]\n')

        end

        function set_fg(obj,fg)
        % % Either set_fg(fg), where fg is appropriate struct
        %   or set_fg(n,nq,np,Bq,theta,actFcnNames)
                obj.fg=fg;
        end

        
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Static
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Static
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Static
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Static
    
    methods(Static=true)
        function f = setpvec(f,pvec)
                f.pvec=pvec;
                Weights = PACNL.getWeightsFromPvec(f.pvec,f.n,f.nu,f.np);
                f.xEnd=Weights.x0;
                f.consts=PACNL.getSysConsts(f);
                f.forward=@(x,u,varargin) PACNL.default_Forward_RNN(x,u,f.pvec,f.n,f.nu,f.np,f.actFcns,varargin);
                if isfield(f,'ce')
                    f.Bq=2*f.ce*(f.consts.Lhv+f.consts.Lv*f.consts.Lhx/(1-f.consts.lambda));
                    f.theta=2*f.ce.*f.consts.Lv*f.consts.Lhx/((1-f.consts.lambda)^2);
                end
        end
        function [xt,yt,A,B,b1,C,D,b2]=default_Forward_RNN(x,u,pvec,n,nu,np,actFcns,varargin)
            if nargin==8
                Weights = PACNL.getWeightsFromPvec(pvec,n,nu,np);
                A=Weights.A;
                B=Weights.B;
                b1=Weights.b1;
                C=Weights.C;
                D=Weights.D;
                b2=Weights.b2;
            else
                A=varargin{1};
                B=varargin{2};
                b1=varargin{3};
                C=varargin{4};
                D=varargin{5};
                b2=varargin{6};
            end

            xt=actFcns{1}.forward(A*x+B*u+b1);
            yt=actFcns{2}.forward(C*x+D*u+b2);

        end
        function [L_hats,f] = empLoss_prototype(loss,f,y,w,Ns_)
            Nmax=(1e6);
            xt=(f.xEnd);

            Weights = PACNL.getWeightsFromPvec(f.pvec,f.n,f.nu,f.np);
            A=(Weights.A);
            B=(Weights.B);
            b1=(Weights.b1);
            C=(Weights.C);
            D=(Weights.D);
            b2=(Weights.b2);
            L_hat=(0);
%             for i=1:size(y,2)
%                 [xt,y_hat_t]=f.forward(xt,w(:,i),A,B,b1,C,D,b2);
%                 L_hat=L_hat+loss(y_hat_t,y(:,i));
%             end

            L_hats=(nan(size(Ns_)));
            start_idx=(1);
            w_=(w);
            y_=(y);
            for s=1:numel(Ns_)
                N_=min(Ns_(s),Nmax);
                for i=start_idx:N_
                    [xt,y_hat_t]=f.forward(xt,w_(:,i),A,B,b1,C,D,b2);
                    L_hat=L_hat+loss(y_hat_t,y_(:,i));
                end
                L_hats(s)=L_hat/N_;
                start_idx=N_+1;
            end
            f.xEnd=xt;
            L_hats=(L_hats);
        end
        function is = isOfClass(f)
            Weights=PACNL.getWeightsFromPvec(f.pvec,f.n,f.nu,f.np);
            is=norm(Weights.A,2).*f.actFcns{1}.lip<1-eps;
        end
        function consts=getSysConsts(f)
            Weights=PACNL.getWeightsFromPvec(f.pvec,f.n,f.nu,f.np);
            l1=f.actFcns{1}.lip;
            l2=f.actFcns{2}.lip;
            
            consts.lambda=l1*norm(Weights.A,2);
            consts.C=1; % or norm(B,2)*(u_infty+norm(b,2))

            %Lv=norm(B,2)
            consts.Lv=norm(Weights.B,2)/l1;
            
            %Lhx=norm(C,2);
            consts.Lhx=l2*norm(Weights.C,2);
            
            %Lhv=norm(D,2);
            consts.Lhv=l2*norm(Weights.D,2);
        end
        function Weights = getWeightsFromPvec(pvec,n,nu,np)
                Weights.x0=pvec(1:n);
                idx=n;
                A=zeros(n,n);
                for i=1:n
                    A(i,:)=pvec(idx+(1:n))';%dot(pvec(idx+(1:n)),x);
                    idx=idx+n;
                end
                Weights.A=A;

                B=zeros(n,nu);
                for i=1:n
                    B(i,:)=pvec(idx+(1:nu))';
                    idx=idx+nu;
                end
                Weights.B=B;

                Weights.b1=pvec(idx+(1:n));
                idx=idx+n;
    
                C=zeros(np,n);
                for i=1:np
                    C(i,:)=pvec(idx+(1:n))';
                    idx=idx+n;
                end
                Weights.C=C;

                D=zeros(np,nu);
                for i=1:np
                    D(i,:)=pvec(idx+(1:nu))';
                    idx=idx+nu;
                end
                Weights.D=D;
                Weights.b2=pvec(idx+(1:np));
        end
        function S = genSys(n,nu,np,actFcns)
                S=struct;
                S.n=n;
                S.np=np;
                S.nu=nu;
                S.pvec=randn(n+n^2+n*nu+n+np*n+np*nu+np,1);
                A=reshape(S.pvec(n+(1:n^2)),n,n);
                A=rand*A./norm(A,2);
                S.pvec(n+(1:n^2))=reshape(A,[],1);
                S.x0=@(pvec) pvec(1:n);
                S.xEnd=S.x0(S.pvec);
                S.actFcns=actFcns;

                S.forward=@(x,u,varargin) PACNL.default_Forward_RNN(x,u,S.pvec,n,nu,np,actFcns,varargin);
                S.consts.lambda=norm(A,2);
                
                S.consts.C=1; % or norm(B,2)*(u_infty+norm(b,2))

                %Lv=norm(B,2)
                S.consts.Lv=norm(reshape(S.pvec((n+n^2)+(1:n*nu)),n,nu),2);
                
                %Lhx=norm(C,2);
                S.consts.Lhx=norm(reshape(S.pvec((n+n^2+n*nu+n)+(1:np*n)),np,n),2);
                
                %Lhv=norm(D,2);
                S.consts.Lhv=norm(reshape(S.pvec((n+n^2+n*nu+n+np*n)+(1:np*nu)),np,nu),2);
        end
        function fg = generate_fg(n,nq,np,Bq,theta,actFcnNames)
%                 n=varargin{1};
%                 nq=varargin{2};
%                 np=varargin{3};
%                 Bq=varargin{4};
%                 theta=varargin{5};
%                 actFcnNames=varargin{6};
            
                SATISFIED=false;
                while SATISFIED==false
                actFcns=cell(2,1);
                for i=1:2
                    actFcns{i}=PACNL.BasisActFcns.(actFcnNames{i});
                end
                fg=PACNL.genSys(n,nq+np,nq+np,actFcns);
                fg.q=nq;
                fg.p=np;
                
                
                x0=[1;fg.pvec];
                A=[-1,zeros(1,numel(fg.pvec))]; % so that ce>0
                b=0;
                options = optimoptions("fmincon",...
                    "Algorithm","interior-point",...
                    "EnableFeasibilityMode",true,...
                    "SubproblemAlgorithm","cg");
                x0=fmincon(@(x) PACNL.CostFcn_setBqTheta1(x,Bq,theta,fg),x0,A,b,[],[],[],[],@(x) PACNL.stability_constaint(x,fg),options);
    
    
                sol=fmincon(@(x) PACNL.CostFcn_setBqTheta1(x,Bq,theta,fg),x0,A,b,[],[],[],[],@(x) PACNL.mycon2(x,Bq,1e-2,theta,1e-3,fg),options);
                   
    
    
    
    
                fg.ce=sol(1);
                fg.pvec=sol(2:end);
                Weights = PACNL.getWeightsFromPvec(fg.pvec,fg.n,fg.nu,fg.np);
                fg.xEnd=Weights.x0;
                fg.consts=PACNL.getSysConsts(fg);
                fg.forward=@(x,u,varargin) PACNL.default_Forward_RNN(x,u,fg.pvec,fg.n,fg.nu,fg.np,fg.actFcns,varargin);
                fg.Bq=2*fg.ce*(fg.consts.Lhv+fg.consts.Lv*fg.consts.Lhx/(1-fg.consts.lambda));
                fg.theta=2*fg.ce.*fg.consts.Lv*fg.consts.Lhx/((1-fg.consts.lambda)^2);
                SATISFIED=fg.ce>0&fg.consts.lambda<1&fg.consts.lambda>0;
                end
        end
        function J=CostFcn_setBqTheta1(theta,Bq_ref,Theta_ref,fg_ref)
            fg_=fg_ref;
            fg_.ce=theta(1);
            fg_.pvec=theta(2:end);
            fg_.consts=PACNL.getSysConsts(fg_);
            
            Lv=fg_.consts.Lv;
            Lhx=fg_.consts.Lhx;
            Lhv=fg_.consts.Lhv;
            lambda=fg_.consts.lambda;
            Bq=2*fg_.ce*(Lhv+Lv*Lhx/(1-lambda));
            Theta=2*fg_.ce.*Lv*Lhx/((1-lambda)^2);
            J=10*(Bq-Bq_ref)^2+100*(Theta-Theta_ref)^2; %-1e-4*(log(1-lambda)+log(lambda))
        end
        function [c,ceq]=stability_constaint(theta,fg_ref)
            fg_=fg_ref;
            fg_.ce=theta(1);
            fg_.pvec=theta(2:end);
            fg_.consts=PACNL.getSysConsts(fg_);
            lambda=fg_.consts.lambda;
            ceq=0;
            c(1)=lambda-0.999;
            c(2)=-lambda+0.01;
        end

        function J=CostFcn_setBqTheta2(theta,fg_ref)
            fg_=fg_ref;
            fg_.ce=theta(1);
            fg_.pvec=theta(2:end);
            fg_.consts=PACNL.getSysConsts(fg_);
            
            Lv=fg_.consts.Lv;
            Lhx=fg_.consts.Lhx;
            Lhv=fg_.consts.Lhv;
            lambda=fg_.consts.lambda;
            J=(fg_.ce-1)^2+(Lhx-1)^2+(Lhv-1)^2+(Lv-1)^2;
        end

        function [c,ceq]=mycon2(theta,Bq_ref,Bq_eps,Theta_ref,Theta_eps,fg_ref)
            fg_=fg_ref;
            fg_.ce=theta(1);
            fg_.pvec=theta(2:end);
            fg_.consts=PACNL.getSysConsts(fg_);
            
            Lv=fg_.consts.Lv;
            Lhx=fg_.consts.Lhx;
            Lhv=fg_.consts.Lhv;
            lambda=fg_.consts.lambda;
            Bq=2*fg_.ce*(Lhv+Lv*Lhx/(1-lambda));
            Theta=2*fg_.ce.*Lv*Lhx/((1-lambda)^2);
            ceq=0;
            c(1)=(Bq-Bq_ref).^2-Bq_eps;
            c(2)=(Theta-Theta_ref).^2-Theta_eps;
            [c_stab,ceq_stab]=PACNL.stability_constaint(theta,fg_ref);
            ceq=[ceq;ceq_stab];
            c=[c;c_stab];
        end
        function [G1,G2u,G2x0] = compNL_Gs(f,fg)
            tl=max(f.g,fg.lambda);
            G=-2/(exp(1)*log(tl));
            Lv_hat=f.M*norm(f.B,2)*norm(f.C,2);

%             C=sqrt(fg.C^2*(1+ (G*f.Lv*fg.Lhx)^2/tl)+f.C) % f.C=matrix
            lambda=sqrt(tl);
            Lv=sqrt(fg.Lv^2+(Lv_hat*G*max(fg.Lhx*fg.Lv,fg.Lhv))^2/(tl^3));
            Lhx=norm(f.C,2);%f.Lhx;
            Lhv=sqrt(2);%sqrt(f.Lhv^2+1);

            G1=Lhv+(2-lambda)*Lv*Lhx/((1-lambda)^2);
            G2x0=f.M*norm(f.C,2)^2*norm(f.B,2)/(1-f.g);
            G2u=2*G2x0*Lv_hat/(1-f.g);
        end
        
        function fe = find_fe(f,fg)
            %find_fe Compute the error system
            %   computes the error system from predictor and data generator in
            %   innovation form.
%             n=fg.n;
            p=fg.p;
            if isfield(fg,'A')==false
                fg=PACNL.getWeightsFromPvec(fg.pvec,fg.n,fg.nu,fg.np);
                f=PACNL.getWeightsFromPvec(f.pvec,f.n,f.nu,f.np);
            end
            C1=fg.C(1:p,:);C2=fg.C(p+1:end,:);
            if (size(f.B,2)==1)
                fe.A=[fg.A,zeros(size(fg.A,1),size(f.A,2)); f.B*C2, f.A];
                fe.B=[fg.B;[zeros(size(f.B,1),p), f.B]];
                fe.C=[C1-f.D*C2, -f.C];
                fe.D=[eye(p,p),-f.D];
            else
            % D1=fg.D(1:p,:);D2=fg.D(p+1:end,:);
                fe.A=[fg.A,zeros(size(fg.A,1),size(f.A,2)); f.B*fg.C, f.A];
                fe.B=[fg.B;f.B];
                fe.C=[C1-f.D*fg.C, -f.C];
                fe.D=[1,0]-f.D;
            
            end
            % fe.A=[fg.A,zeros(size(fg.A,1),size(f.A,2)); f.B*C2, f.A];
            % fe.B=[fg.B;[f.B]];
            % fe.C=[C1-f.D*C2, -f.C];
            % fe.D=[[1,0]-f.D];
        end

        function n = normL1(ss)
            if isfield(ss,'A')==false
                ss=PACNL.getWeightsFromPvec(ss.pvec,ss.n,ss.nu,ss.np);
            end
            tol=1e-7;
            lim=1e5;
            
            n=norm(ss.D,2);
            new_n=n+norm(ss.C*ss.B,2);
            old_AkB=ss.B;
            k=0;
            while (abs(new_n-n)>tol && k<=lim) || k<50
                n=new_n;
                old_AkB=ss.A*old_AkB;
                k=k+1;
                new_n=n+norm(ss.C*old_AkB,2);
            end
            n=new_n;
        end
        function n = normL1_alternative(ss)
            %UNTITLED3 Summary of this function goes here
            %   Detailed explanation goes here
            if isfield(ss,'A')==false
                ss=PACNL.getWeightsFromPvec(ss.pvec,ss.n,ss.nu,ss.np);
            end
            tol=1e-7;
            lim=1e5;
            
            n=norm(ss.D,2);
            new_n=n+2*norm(ss.C*ss.B,2); %new_n=|D|+|CA^0B|
            old_AkB=ss.B;
            k=1;
            while (abs(new_n-n)>tol && k<=lim) || k<50
                n=new_n;
                old_AkB=ss.A*old_AkB;
                k=k+1;
                new_n=n+(k+1)*norm(ss.C*old_AkB,2);
            end
            n=new_n;
        end
        function [Gf_new,M,g,barGf1,barGf2] = Gf_new(varargin)
            %Gf_new computes G_f for bounded case as stated in the paper
            %   Uses:
            %  [Gf] = Gf(f),
            %            f=struct, with f.A,f.B,f.C,f.D defined
            %  [Gf] = Gf(f,\hat{M},\hat{\gamma}),
            %             f=struct, with f.A,f.B,f.C,f.D defined
            %             \hat{M},\hat{\gamma} are scalars s.t.
            %             |f.A^k|<=\hat{M}*\hat{\gamma}^k
            % Optionally: 
            %   [Gf,M,g] = Gf(f),
            %   also returns M=\hat{M},g=\hat{\gamma}, s.t. 
            %   M,g = argmin M/(1-g), and  norm(f.A^k,2)<=M*g^k, for k<=20
            %   while bound for k<=20 is guaranteed, constrained are tightened to
            %   ensure it holds for higher k, but it is not guaranteed.
            
            
            f=varargin{1};
            if nargin==1
                if isfield(f,'A')==false
                    f=PACNL.getWeightsFromPvec(f.pvec,f.n,f.nu,f.np);
                end
                [M,g]=PACNL.findMgammaOpt(f.A);
            else
                M=varargin{2};
                g=varargin{3};
            end
            MCB=M*norm(f.C,2)*norm(f.B,2);
            Gf_new=(MCB/((1-g)^(3/2)))*(1+norm(f.D,2)+MCB/(1-g));
            barGf1=MCB/(1-g);
            barGf2=(1+norm(f.D,2)+MCB/(1-g))/(1-g);
        end
        function [M,gamma] = findMgammaOpt(A)
            %UNTITLED7 find M,gamma s.t. |A^k|<= M*gamma^k
            %   Detailed explanation goes here
            
            empirical_k=20;
            
            seq=nan(1,empirical_k+1);
            seq(1)=1;%norm(A,2);
            tempA=eye(size(A));%A;
            for j=2:empirical_k+1
            tempA=tempA*A;
            seq(j)=norm(tempA,2);
            end
            gamma_star=max(abs(eig(A)));
            
            %seq=[|A^0|,|A^1|,|A^2|,... 
            seq2=gamma_star.^(0:empirical_k);
            M_star=max(1,max(seq./seq2));
            
            
            
            
            
            A_ineq=[0,1;-1,0;0,-1];
            B_ineq=[1;-1;-gamma_star];
            
            
            powers=seq(log(seq)>-600)'; % |A^k| can be so small that log(|A^k|)~=log(0)=-inf, which messes fmincon up.
            
            
            options=optimoptions('fmincon','Display','off');
            try
            sol=fmincon(@J1,[M_star,gamma_star],A_ineq,B_ineq,[],[],[],[],@(X) NONLCON(X,powers),options);
            catch ME
                fprintf(ME.message)
            end
            M=sol(1)+10*eps(sol(1));
            gamma=sol(2)+10*eps(sol(2));
            
            

            
            function [f,g]=J1(X)
                f=X(1)/((1-X(2)));
                g=[1/((1-X(2))); X(1)/((X(2)-1)^2)];
            end
            
            
            
            
            function [C,Ceq]=NONLCON(X,powers)
            % Goal: for all k: |A^k|<=Mg^k
            % => |A^k|/(Mg^k)<=1-eps
            % if x<y then log(x) <log(y)
            % => log(|A^k|)-log(Mg^k)<=log(1-eps)
            % => log(|A^k|)-log(Mg^k)-log(1-eps)<=0
            %     C=powers-X(1).*(X(2).^(0:numel(powers)-1))';
            eps=5e-4;
                C=log(powers)-log(X(1))-log(X(2)).*(0:(numel(powers)-1))';
                C=C-log(1-eps).*(0:(numel(powers)-1))';
                Ceq=[];
            end
        end
        function [f_end,Q,acc,rej,Fs] = customMH_parfor(f0,n,lpos,proprnd_fcn,thin,N_threads,quant,acc,rej)
        %customMH_parfor Metropolis-Hastings algorithm for sampling
        %predictors
        % f0 is initial point
        % n  is number of samples to generate
        % lpos is the log-likelihood function 
        % proprnd_fcn is the propogation function (must be symmetric)
        % thin - thinning parameter, how many samples to skip
        % N_threads - how many threads to use
        % quant - a collumn cell array of functions to evaluate the MCMC samples on
        %         i.e. quant= {@(x) x^2; @(x)sqrt(x); @(x)some_function(x)};
        % acc - how many samples have been accepted before (to continue this
        %       statistic)
        % rej - how many samples have been rejected before
        %
        % f_end - the last monte-carlo sample, to be used as f0 in next fcn call
        % Q - is (numel(quant) by n) matrix containing the function valuations
        % acc,rej - acceptance rate statistic
        
        
        fs=cell(N_threads,1);
        old_lpos=cell(N_threads,1);
        n_quant=numel(quant);
        Quants=cell(N_threads);
        % acc=zeros(N_threads,1);
        % rej=zeros(N_threads,1);
        
        
        % lu=log(rand(1,n));
        
        %% check initial condition
        old_lpos0=lpos(f0);
        
        attempts=0;
        fp=f0;
        while isinf(old_lpos0)&&attempts<1000 % invalid initial point, seeing if there's valid points in the neighborhood
            fp=proprnd_fcn(f0);
            [old_lpos0,~,fp]=lpos(fp);
            attempts=attempts+1;
        end
        
        if isinf(lpos(fp))
            error('Invalid initial point')
        elseif (isinf(lpos(f0)))
            warning('Invalid initial point, but found a valid point in the neighborhood')
            f0=fp;
        end
        
        %% Burn-in (default 10% of n)
        % Nf_burnin=ceil(n/10);
        % lu_th=log(rand(1,Nf_burnin*thin+thin));
        % f=f0;
        % old_lpos=old_lpos0;
        % for i=1:Nf_burnin
        %     for thin_i=1:thin
        %         fp=proprnd_fcn(f);
        %         [lp,~,fp]=lpos(fp);
        %     
        %         lH=lp-old_lpos;
        %         if lu_th(thin*i+thin_i)<lH 
        %             %keep
        %             f=fp;
        %             old_lpos=lp;
        %         else
        %             %don't keep
        %         end
        %     end
        % end
        % f0=f;
        % old_lpos0=old_lpos;
        %% Main loop
        
        f_s_idx=cell(1,N_threads);
        old_lpos=cell(1,N_threads);
        f_s=cell(1,N_threads);
        for i=1:N_threads
            fs{i}=f0;
            old_lpos{i}=old_lpos0;
            Quants_{i}=nan(n_quant,floor(n/N_threads));
            f_s{i}=cell(1,floor(n/N_threads));
            f_s_idx{i}=0;
        end
        
        acc=ones(1,N_threads)*acc;
        rej=ones(1,N_threads)*rej;
        
        parfor th=1:N_threads
            lu_th=log(rand(1,floor(n/N_threads)*thin+thin));
        
            do=floor(n/N_threads);
            if th==N_threads
                do=n-floor(n/N_threads)*(N_threads-1)
            end
            lu_th=log(rand(1,do*thin+thin));
        
            for i=1:do
                f=fs{th};
                for thin_i=1:thin
                    fp=proprnd_fcn(f);
                    [lp,~,fp]=lpos(fp);
                
                    lH=lp-old_lpos{th};
                    if lu_th(thin*i+thin_i)<lH 
                        %keep
                        f=fp;
                        old_lpos{th}=lp;
                        acc(th)=acc(th)+1;
                    else
                        rej(th)=rej(th)+1;
                        %don't keep
                    end
                end
                fs{th}=f;
                Quants_{th}(:,i)=cellfun(@(fun) fun(f),quant);%quant(f);
                f_s_idx{th}=f_s_idx{th}+1;
                f_s{th}{f_s_idx{th}}=f;
                
            end
        %     fprintf('th:%i, acc_rate=%f\n',th,acc(th)/(acc(th)+rej(th)));
        end
        
        nth=floor(n/N_threads);
        Q=nan(n_quant,n);
        Fs=cell(1,n);
        for i=1:N_threads
        %     nth=numel(f_s{i});
            idx=(i-1)*nth+1;
            Fs(idx:(idx+numel(f_s{i})-1))=f_s{i};
            Q(:,idx:(idx+numel(f_s{i})-1))=Quants_{i};
        end
        f_end=fs{randi(N_threads)};
        
        acc=sum(acc);
        rej=sum(rej);
        
        end
    end
end


function y=tanh(x)
    v=exp(2.*x);
    y=(v-1)./(v+1);
end